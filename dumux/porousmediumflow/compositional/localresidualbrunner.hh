// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup PorousmediumflowModels
 * \brief Element-wise calculation of the local residual for problems
 *        using compositional fully implicit model.
 */

#ifndef DUMUX_COMPOSITIONAL_LOCAL_RESIDUAL_HH
#define DUMUX_COMPOSITIONAL_LOCAL_RESIDUAL_HH

#include <vector>
#include <dune/common/exceptions.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/flux/referencesystemformulation.hh>

namespace Dumux {

/*!
 * \ingroup PorousmediumflowModels
 * \brief Element-wise calculation of the local residual for problems
 *        using compositional fully implicit model.
 */
template<class TypeTag>
class CompositionalLocalResidual: public GetPropType<TypeTag, Properties::BaseLocalResidual>
{
    using ParentType = GetPropType<TypeTag, Properties::BaseLocalResidual>;
    using Implementation = GetPropType<TypeTag, Properties::LocalResidual>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using NumEqVector = Dumux::NumEqVector<GetPropType<TypeTag, Properties::PrimaryVariables>>;
    using FluxVariables = GetPropType<TypeTag, Properties::FluxVariables>;
    using ElementFluxVariablesCache = typename GetPropType<TypeTag, Properties::GridFluxVariablesCache>::LocalView;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using EnergyLocalResidual = GetPropType<TypeTag, Properties::EnergyLocalResidual>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using Indices = typename ModelTraits::Indices;
 enum
    {

        wPhaseIdx = 0,
        nPhaseIdx = 1,
        CalPhaseIdx=0, //
        minAPhaseIdx=1, //
        minBPhaseIdx=2,  //


        H2OIdx=0, //
        CO2Idx=1, //
        HIdx=2, //
        CaIdx=3,
        MeIdx=4,
        SiO2Idx=5,
        CalIdx=6,
        minAIdx=7,
        minBIdx=8
    };

    static constexpr int numPhases = ModelTraits::numFluidPhases();
    static constexpr int numComponents = ModelTraits::numFluidComponents();
    static constexpr bool useMoles = ModelTraits::useMoles();

    enum { conti0EqIdx = Indices::conti0EqIdx };

    //! The index of the component balance equation that gets replaced with the total mass balance
    static constexpr int replaceCompEqIdx = ModelTraits::replaceCompEqIdx();
    static constexpr bool useTotalMoleOrMassBalance = replaceCompEqIdx < numComponents;

public:
    using ParentType::ParentType;

    /*!
     * \brief Evaluates the amount of all conservation quantities
     *        (e.g. phase mass) within a sub-control volume.
     *
     * The result should be averaged over the volume (e.g. phase mass
     * inside a sub control volume divided by the volume)
     *
     * \param problem The problem
     * \param scv The sub control volume
     * \param volVars The current or previous volVars
     */
    NumEqVector computeStorage(const Problem& problem,
                               const SubControlVolume& scv,
                               const VolumeVariables& volVars) const
    {
NumEqVector storage(0.0);
        // std::cout<<"localresidual fan"<<std::endl;


        Scalar Phi = volVars.porosity();
        Scalar Sl  = volVars.saturation(wPhaseIdx);
        Scalar Sg  = volVars.saturation(nPhaseIdx);

        Scalar rhoW= volVars.molarDensity(wPhaseIdx);
        Scalar rhoN= volVars.molarDensity(nPhaseIdx);

        // mole fraction components of aqeuous phase
        Scalar cH2Ol = volVars.moleFraction(wPhaseIdx,  H2OIdx)*rhoW;
        Scalar cCO2l = volVars.moleFraction(wPhaseIdx,  CO2Idx)*rhoW;
        Scalar cH = volVars.moleFraction(wPhaseIdx, HIdx)*rhoW;
        Scalar cCa = volVars.moleFraction(wPhaseIdx, CaIdx)*rhoW;
        Scalar cMe = volVars.moleFraction(wPhaseIdx, MeIdx)*rhoW;
        Scalar cSiO2 = volVars.moleFraction(wPhaseIdx, SiO2Idx)*rhoW;
        Scalar cHCO3 =1e-3*cCO2l/cH;

        // // minerals :
         Scalar Cal = volVars.precipitateConcentration(CalPhaseIdx);
         Scalar minA = volVars.precipitateConcentration(minAPhaseIdx);
         Scalar minB = volVars.precipitateConcentration(minBPhaseIdx);


         // std::cout<<cCO2l<<"  "<<cH<<"  "<<cCa<<"  "<<cMe<<"  "<<cSiO2<<" " <<cHCO3<<std::endl;
         // std::cout<<Cal<<"  "<<minA<<"  "<<minB<<std::endl;

        // mole fraction components of gaz phase
        Scalar cH2Og = volVars.moleFraction(nPhaseIdx, H2OIdx)*rhoN;
        Scalar cCO2g = volVars.moleFraction(nPhaseIdx,   CO2Idx)*rhoN;



       // Total concentration for liquid components
        Scalar TwH2O = cH2Ol + cHCO3 + Cal + minB;
        Scalar TwCO2 = cCO2l + cHCO3  + Cal + minB;
        Scalar TwH =  cH  - cHCO3 -2*Cal-3*minA-3*minB;
        Scalar TwCa =  cCa +Cal;
        Scalar TwMe =  cMe +minA+minB;
        Scalar TwSiO2 =  cSiO2 +minA;

        // Total concentration for gas components
        Scalar TH2Og = cH2Og;
        Scalar TCO2g = cCO2g;



        // STORAGE of components
        storage[H2OIdx]+= Phi*Sl*TwH2O;// + Phi*Sg*TH2Og          ;
        storage[CO2Idx]+= Phi*Sl*TwCO2  + Phi*Sg*TCO2g         ;
        storage[HIdx]  += Phi*Sl*TwH                               ;
        storage[CaIdx] += Phi*Sl*TwCa                              ;
        storage[MeIdx] += Phi*Sl*TwMe                              ;
        storage[SiO2Idx] += Phi*Sl*TwSiO2                          ;

        storage[CalIdx] = 0.,
        storage[minAIdx] = 0.;
        storage[minBIdx] = 0.;

        // std::cout<<"storage"<<storage<<std::endl;

        // energy part
         for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
             {

            //! The energy storage in the fluid phase with index phaseIdx
            EnergyLocalResidual::fluidPhaseStorage(storage, scv, volVars, phaseIdx);
        }
           //! The energy storage in the solid matrix
         EnergyLocalResidual::solidPhaseStorage(storage, scv, volVars);
         //  std::cout<<storage<<std::endl;
         
         return storage;        
    }

    /*!
     * \brief Evaluates the total flux of all conservation quantities
     *        over a face of a sub-control volume.
     *
     * \param problem The problem
     * \param element The current element.
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars The volume variables of the current element
     * \param scvf The sub control volume face to compute the flux on
     * \param elemFluxVarsCache The cache related to flux compuation
     */
    NumEqVector computeFlux(const Problem& problem,
                            const Element& element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolumeFace& scvf,
                            const ElementFluxVariablesCache& elemFluxVarsCache) const
    {
        FluxVariables fluxVars;
        fluxVars.init(problem, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);
        static constexpr auto referenceSystemFormulation = FluxVariables::MolecularDiffusionType::referenceSystemFormulation();
        // get upwind weights into local scope
        NumEqVector flux(0.0);

        const auto MoleDensity = [](const auto& volVars, const int phaseIdx)
        { return volVars.molarDensity(phaseIdx); };

        const auto MoleFraction= [](const auto& volVars, const int phaseIdx, const int compIdx)
        { return volVars.moleFraction(phaseIdx, compIdx); };

         const auto Total= [](const auto& volVars, const int phaseIdx,  const int compIdx)
            {
                NumEqVector total(0.0);
               
                Scalar xH2Ol =  volVars.moleFraction(phaseIdx, H2OIdx);
                Scalar xCO2l =  volVars.moleFraction(phaseIdx, CO2Idx);
                Scalar xH    =  volVars.moleFraction(phaseIdx, HIdx  );
                Scalar xCa   =  volVars.moleFraction(phaseIdx, CaIdx );
                Scalar xMe   =  volVars.moleFraction(phaseIdx, MeIdx );
                Scalar xSiO2 =  volVars.moleFraction(phaseIdx, SiO2Idx );


                Scalar xHCO3 = 1e-3*xCO2l/xH/volVars.molarDensity(phaseIdx);

                total[0] =(xH2Ol + xHCO3);
                total[1] = (xCO2l + xHCO3);
                total[2] =  (xH  - xHCO3 );
                total[3] =xCa;
                total[4] =xMe;
                total[5] =xSiO2;
               
                return total[compIdx];
               

            };

        // advective fluxes

int phaseIdx = 0;//liquid phase

        for (int compIdx = 0; compIdx < numComponents; ++compIdx)
            {


        // the physical quantities for which we perform upwinding
                const auto upwindTerm = [&MoleDensity, &Total, phaseIdx, compIdx] (const auto& volVars)
                {
                    //std::cout<< MoleDensity(volVars, phaseIdx)*Total(volVars, phaseIdx, compIdx)*volVars.mobility(phaseIdx)<<std::endl;
                    return MoleDensity(volVars, phaseIdx)*Total(volVars, phaseIdx, compIdx)*volVars.mobility(phaseIdx); };
               
                flux[compIdx] +=fluxVars.advectiveFlux(phaseIdx,upwindTerm);
              
            }
       
        phaseIdx = 1;//gas phase
        for (int compIdx = 1; compIdx < 2; ++compIdx) //only advective flux for CO2 in gaz phase
            {


                // the physical quantities for which we perform upwinding
                const auto upwindTerm = [&MoleDensity,  &MoleFraction, phaseIdx, compIdx] (const auto& volVars)
                    { return MoleDensity(volVars, phaseIdx)*MoleFraction(volVars, phaseIdx, compIdx)*volVars.mobility(phaseIdx); };
                
                flux[compIdx] +=fluxVars.advectiveFlux(phaseIdx,upwindTerm);

            }
    
        // diffusive fluxes

        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
            {

                const auto diffusiveFluxes = fluxVars.molecularDiffusionFlux(phaseIdx);
                for (int compIdx = 0; compIdx < numComponents; ++compIdx)
                    {
                        //check for the reference system and adapt units of the diffusive flux accordingly.
                        flux[compIdx] += diffusiveFluxes[compIdx];///FluidSystem::molarMass(compIdx);
                    }
            }

        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
        {  //! Add advective phase energy fluxes. For isothermal model the contribution is zero.
            EnergyLocalResidual::heatConvectionFlux(flux, fluxVars, phaseIdx);
        }
        
        //! Add diffusive energy fluxes. For isothermal model the contribution is zero.
        EnergyLocalResidual::heatConductionFlux(flux, fluxVars);


        return flux;
         
    }
NumEqVector computeSource(const Problem& problem,
                              const Element& element,
                              const FVElementGeometry& fvGeometry,
                              const ElementVolumeVariables& elemVolVars,
                              const SubControlVolume &scv) const
    {
        NumEqVector source(0.0);

        // add contributions from volume flux sources
        source += problem.source(element, fvGeometry, elemVolVars, scv);



        const VolumeVariables &volVars = elemVolVars[scv];

        Scalar rhoW= volVars.molarDensity(wPhaseIdx);


        Scalar cCO2l = volVars.moleFraction(wPhaseIdx,  CO2Idx)*rhoW;
        Scalar cH = volVars.moleFraction(wPhaseIdx, HIdx)*rhoW;
        Scalar cCa = volVars.moleFraction(wPhaseIdx, CaIdx)*rhoW;
        Scalar cMe = volVars.moleFraction(wPhaseIdx, MeIdx)*rhoW;
        Scalar cSiO2 = volVars.moleFraction(wPhaseIdx, SiO2Idx)*rhoW;
        Scalar cHCO3 =1e-3*cCO2l/cH;


        //  minerals
         Scalar Cal = volVars.precipitateConcentration(CalPhaseIdx);
         Scalar minA = volVars.precipitateConcentration(minAPhaseIdx);
         Scalar minB = volVars.precipitateConcentration(minBPhaseIdx);


         
         double rcal=1.-1.*cHCO3*cCa/cH;
         double rminA=1.-1e-3*cMe*cSiO2/pow(cH,3.);
         double rminB=1.-0.8*cHCO3*cMe/pow(cH,2.);
        

         source[CalIdx]=std::fmin(rcal,Cal);
         source[minAIdx]= std::fmin(rminA,minA);
         source[minBIdx]=std::fmin(rminB,minB);

         return source;
    }
protected:
    Implementation *asImp_()
    { return static_cast<Implementation *> (this); }

    const Implementation *asImp_() const
    { return static_cast<const Implementation *> (this); }
};

} // end namespace Dumux

#endif
