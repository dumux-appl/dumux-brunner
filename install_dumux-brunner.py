#!/usr/bin/env python3

# 
# This installs the module dumux-brunner and its dependencies.
# The exact revisions used are listed in the table below.
# However, note that this script may also apply further patches.
# If so, all patches are required to be the current folder, or,
# in the one that you specified as argument to this script.
# 
# 
# |      module name      |      branch name      |                 commit sha                 |         commit date         |
# |-----------------------|-----------------------|--------------------------------------------|-----------------------------|
# |      dune-common      |  origin/releases/2.7  |  aa689abba532f40db8f5663fa379ea77211c1953  |  2020-11-10 13:36:21 +0000  |
# |       dune-grid       |  origin/releases/2.7  |  e8b460122a411cdd92f180dcca81ad8b2ac8d5ee  |  2021-08-31 13:04:20 +0000  |
# |       dune-istl       |  origin/releases/2.7  |  761b28aa1deaa786ec55584ace99667545f1b493  |  2020-11-26 23:29:21 +0000  |
# |     dumux-brunner     |     origin/master     |  ba3f384d2532935270d4341aae82efc785dd6618  |  2022-09-29 12:37:11 +0200  |
# |      dune-alugrid     |  origin/releases/2.7  |  51bde29a2dfa7cfac4fb73d40ffd42b9c1eb1d3d  |  2021-04-22 15:10:17 +0200  |
# |  dune-localfunctions  |  origin/releases/2.7  |  68f1bcf32d9068c258707d241624a08b771b6fde  |  2020-11-26 23:45:36 +0000  |
# |         dumux         |  origin/releases/3.4  |  f78393cff93e246d736819d9f6efc261ef733a1f  |  2021-11-30 18:47:58 +0000  |
# |     dune-geometry     |  origin/releases/2.7  |  9d56be3e286bc761dd5d453332a8d793eff00cbe  |  2020-11-26 23:26:48 +0000  |

import os
import sys
import subprocess

top = "DUMUX"
os.makedirs(top, exist_ok=True)


def runFromSubFolder(cmd, subFolder):
    folder = os.path.join(top, subFolder)
    try:
        subprocess.run(cmd, cwd=folder, check=True)
    except Exception as e:
        cmdString = ' '.join(cmd)
        sys.exit(
            "Error when calling:\n{}\n-> folder: {}\n-> error: {}"
            .format(cmdString, folder, str(e))
        )


def installModule(subFolder, url, branch, revision):
    targetFolder = url.rstrip(".git").split("/")[-1]
    if not os.path.exists(targetFolder):
        runFromSubFolder(['git', 'clone', url, targetFolder], '.')
        runFromSubFolder(['git', 'checkout', branch], subFolder)
        runFromSubFolder(['git', 'reset', '--hard', revision], subFolder)
    else:
        print(
            f"Skip cloning {url} since target '{targetFolder}' already exists."
        )


def applyPatch(subFolder, patch):
    sfPath = os.path.join(top, subFolder)
    patchPath = os.path.join(sfPath, 'tmp.patch')
    with open(patchPath, 'w') as patchFile:
        patchFile.write(patch)
    runFromSubFolder(['git', 'apply', 'tmp.patch'], subFolder)
    os.remove(patchPath)

print("Installing dune-common")
installModule("dune-common", "https://gitlab.dune-project.org/core/dune-common.git", "origin/releases/2.7", "aa689abba532f40db8f5663fa379ea77211c1953", )

print("Installing dune-grid")
installModule("dune-grid", "https://gitlab.dune-project.org/core/dune-grid", "origin/releases/2.7", "e8b460122a411cdd92f180dcca81ad8b2ac8d5ee", )

print("Installing dune-istl")
installModule("dune-istl", "https://gitlab.dune-project.org/core/dune-istl.git", "origin/releases/2.7", "761b28aa1deaa786ec55584ace99667545f1b493", )

print("Installing dumux-brunner")
installModule("dumux-brunner", "https://git.iws.uni-stuttgart.de/dumux-appl/dumux-brunner.git", "origin/master", "ba3f384d2532935270d4341aae82efc785dd6618", )

print("Installing dune-alugrid")
installModule("dune-alugrid", "https://gitlab.dune-project.org/extensions/dune-alugrid.git", "origin/releases/2.7", "51bde29a2dfa7cfac4fb73d40ffd42b9c1eb1d3d", )

print("Installing dune-localfunctions")
installModule("dune-localfunctions", "https://gitlab.dune-project.org/core/dune-localfunctions.git", "origin/releases/2.7", "68f1bcf32d9068c258707d241624a08b771b6fde", )

print("Installing dumux")
installModule("dumux", "https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git", "origin/releases/3.4", "f78393cff93e246d736819d9f6efc261ef733a1f", )

print("Installing dune-geometry")
installModule("dune-geometry", "https://gitlab.dune-project.org/core/dune-geometry.git", "origin/releases/2.7", "9d56be3e286bc761dd5d453332a8d793eff00cbe", )

print("Applying patch for uncommitted changes in dune-common")
patch = """
diff --git a/dune/common/densematrix.hh b/dune/common/densematrix.hh
index 096f3f4d8..fd0465ceb 100644
--- a/dune/common/densematrix.hh
+++ b/dune/common/densematrix.hh
@@ -935,9 +935,26 @@ namespace Dune
 
       // singular ?
       nonsingularLanes = nonsingularLanes && (pivmax != real_type(0));
-      if (throwEarly) {
-        if(!Simd::allTrue(nonsingularLanes))
-          DUNE_THROW(FMatrixError, \"matrix is singular\");
+      if (throwEarly)
+      {
+         if(!Simd::allTrue(nonsingularLanes))
+         {
+            //DUNE_THROW(FMatrixError, \"matrix is singular\");
+            std::cout<<\"matrix is singular OK\"<<std::endl;
+            std::cout<< \"A singular\"<<std::endl;
+
+            for (int i = 0; i < A.rows(); i++)
+            {
+                for (int j = 0; j < A.rows(); j++)
+                {
+                    std::cout << A[i][j] << \" \";
+                }
+                // Newline for new row
+                std::cout << std::endl;
+            }
+
+          return;
+          }
       }
       else { // !throwEarly
         if(!Simd::anyTrue(nonsingularLanes))
"""
applyPatch("dune-common", patch)

print("Applying patch for uncommitted changes in dune-istl")
patch = """
diff --git a/dune/istl/solver.hh b/dune/istl/solver.hh
index 0e100512..e96f988a 100644
--- a/dune/istl/solver.hh
+++ b/dune/istl/solver.hh
@@ -456,9 +456,9 @@ namespace Dune
           if (_parent._verbose>0)
             std::cout << \"=== \" << _parent.name() << \": abort due to infinite or NaN defect\"
                       << std::endl;
-          DUNE_THROW(SolverAbort,
-                     _parent.name() << \": defect=\" << Simd::io(def)
-                     << \" is infinite or NaN\");
+          // DUNE_THROW(SolverAbort,
+                     // _parent.name() << \": defect=\" << Simd::io(def)
+                     // << \" is infinite or NaN\");
         }
         if(i == 0)
           _def0 = def;
diff --git a/dune/istl/solvers.hh b/dune/istl/solvers.hh
index 5212e1a2..8ebbdc24 100644
--- a/dune/istl/solvers.hh
+++ b/dune/istl/solvers.hh
@@ -492,13 +492,25 @@ namespace Dune {
 
         // look if breakdown occurred
         if (Simd::allTrue(abs(rho) <= EPSILON))
-          DUNE_THROW(SolverAbort,\"breakdown in BiCGSTAB - rho \"
-                     << Simd::io(rho) << \" <= EPSILON \" << EPSILON
-                     << \" after \" << it << \" iterations\");
+          { std::cout<<\"breakdown in BiCGSTAB - rho \"
+                     << rho << \" <= EPSILON \" << max_value(EPSILON)
+                     << \" after \" << it << \" iterations\"<<std::endl;
+            res.converged = 0;
+            return;
+          }
+        // DUNE_THROW(SolverAbort,\"breakdown in BiCGSTAB - rho \"
+           //           << Simd::io(rho) << \" <= EPSILON \" << EPSILON
+           //           << \" after \" << it << \" iterations\");
         if (Simd::allTrue(abs(omega) <= EPSILON))
-          DUNE_THROW(SolverAbort,\"breakdown in BiCGSTAB - omega \"
-                     << Simd::io(omega) << \" <= EPSILON \" << EPSILON
-                     << \" after \" << it << \" iterations\");
+          { std::cout<<\"breakdown in BiCGSTAB - omega \"
+                     << omega << \" <= EPSILON \" << max_value(EPSILON)
+                     << \" after \" << it << \" iterations\"<<std::endl;
+            res.converged = 0;
+            return;
+          } 
+            //DUNE_THROW(SolverAbort,\"breakdown in BiCGSTAB - omega \"
+            //       << Simd::io(omega) << \" <= EPSILON \" << EPSILON
+        //       << \" after \" << it << \" iterations\");
 
 
         if (it<1)
@@ -522,9 +534,15 @@ namespace Dune {
         h = _sp->dot(rt,v);
 
         if ( Simd::allTrue(abs(h) < EPSILON) )
-          DUNE_THROW(SolverAbort,\"abs(h) < EPSILON in BiCGSTAB - abs(h) \"
-                     << Simd::io(abs(h)) << \" < EPSILON \" << EPSILON
-                     << \" after \" << it << \" iterations\");
+         { std::cout<<\"breakdown in BiCGSTAB - h \"
+                     << abs(h) << \" <= EPSILON \" << max_value(EPSILON)
+                     << \" after \" << it << \" iterations\"<<std::endl;
+            res.converged = 0;
+            return;
+          }
+        // DUNE_THROW(SolverAbort,\"abs(h) < EPSILON in BiCGSTAB - abs(h) \"
+        //              << Simd::io(abs(h)) << \" < EPSILON \" << EPSILON
+        //              << \" after \" << it << \" iterations\");
 
         alpha = rho_new / h;
 
@@ -540,6 +558,15 @@ namespace Dune {
         //
 
         norm = _sp->norm(r);
+
+        if (norm != norm) //detection NaN
+         { std::cout<<\"breakdown in BiCGSTAB - norm \"
+                     <<norm << \" is NAN  after \" << it << \" iterations\"<<std::endl;
+            res.converged = 0;
+            return;
+         } 
+
+        
         if(iteration.step(it, norm)){
           break;
         }
"""
applyPatch("dune-istl", patch)

print("Applying patch for uncommitted changes in dumux")
patch = """
diff --git a/dumux/assembly/cclocalassembler.hh b/dumux/assembly/cclocalassembler.hh
index 908d992cb..408fd9b7e 100644
--- a/dumux/assembly/cclocalassembler.hh
+++ b/dumux/assembly/cclocalassembler.hh
@@ -107,20 +107,6 @@ public:
         this->asImp_().bindLocalViews();
         const auto globalI = this->assembler().gridGeometry().elementMapper().index(this->element());
         res[globalI] = this->asImp_().evalLocalResidual()[0]; // forward to the internal implementation
-
-        using Problem = GetPropType<TypeTag, Properties::Problem>;
-        if constexpr (Problem::enableInternalDirichletConstraints())
-        {
-            const auto applyDirichlet = [&] (const auto& scvI,
-                                             const auto& dirichletValues,
-                                             const auto eqIdx,
-                                             const auto pvIdx)
-            {
-                res[scvI.dofIndex()][eqIdx] = this->curElemVolVars()[scvI].priVars()[pvIdx] - dirichletValues[pvIdx];
-            };
-
-            this->asImp_().enforceInternalDirichletConstraints(applyDirichlet);
-        }
     }
 };
 
diff --git a/dumux/multidomain/embedded/couplingmanager1d3d_average.hh b/dumux/multidomain/embedded/couplingmanager1d3d_average.hh
index 8972ff74b..aa405034e 100644
--- a/dumux/multidomain/embedded/couplingmanager1d3d_average.hh
+++ b/dumux/multidomain/embedded/couplingmanager1d3d_average.hh
@@ -253,11 +253,6 @@ public:
                     }
                 }
 
-                // if the circle stencil is empty (that is the circle is entirely outside the domain)
-                // we do not add a (zero) point source
-                if (circleStencil.empty())
-                    continue;
-
                 // export low dim circle stencil
                 if constexpr (isBox<bulkIdx>())
                 {
diff --git a/dumux/multidomain/subdomaincclocalassembler.hh b/dumux/multidomain/subdomaincclocalassembler.hh
index 74a98682e..2d9d86574 100644
--- a/dumux/multidomain/subdomaincclocalassembler.hh
+++ b/dumux/multidomain/subdomaincclocalassembler.hh
@@ -161,19 +161,6 @@ public:
         this->asImp_().bindLocalViews();
         const auto globalI = this->fvGeometry().gridGeometry().elementMapper().index(this->element());
         res[globalI] = this->evalLocalResidual()[0]; // forward to the internal implementation
-
-        if constexpr (Problem::enableInternalDirichletConstraints())
-        {
-            const auto applyDirichlet = [&] (const auto& scvI,
-                                             const auto& dirichletValues,
-                                             const auto eqIdx,
-                                             const auto pvIdx)
-            {
-                res[scvI.dofIndex()][eqIdx] = this->curElemVolVars()[scvI].priVars()[pvIdx] - dirichletValues[pvIdx];
-            };
-
-            this->asImp_().enforceInternalDirichletConstraints(applyDirichlet);
-        }
     }
 
     /*!
"""
applyPatch("dumux", patch)

print("Configuring project")
runFromSubFolder(
    ['./dune-common/bin/dunecontrol', '--opts=dumux-brunner/cmake.opts', 'all'],
    '.'
)
