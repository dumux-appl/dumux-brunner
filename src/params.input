[TimeLoop]
DtInitial = 0.05                    # [s]
TEnd = 2e6                      # [s]
MaxTimeStepSize = 1e6               # [s]

[TimeScheme]
Order = 2                      # [-] Order of Euler time scheme

[Restart]
### Sequential Test
#Time = 49760.9                     # [s] Restart Time
#File = co2-00010.vtu               # [s] Restart File
#Iteration = 10                     # [-] Restart at iteration number
### Parallel Test
#Time = 185431                      # [s] Restart Time
#File = s0002-co2-00250.pvtu        # [s] Restart File
#Iteration = 250                    # [-] Restart at iteration number

[Grid]
#File = grids/brunner_mesh2D.msh     # relative path to the grid file
File = grids/brunner600.msh        # relative path to the grid file

[FluidSystem]
NTemperature =  100                 # [-] number of tabularization entries
NPressure = 100                     # [-] number of tabularization entries
PressureLow = 1e5                   # [Pa] low end for tabularization of fluid properties
PressureHigh = 3e7                  # [Pa] high end for tabularization of fluid properties
TemperatureLow = 273.5              # [Pa] low end for tabularization of fluid properties
TemperatureHigh = 330               # [Pa] high end for tabularization of fluid properties

[Problem]
Name = co2                          # [-] the name of the output files
EnableGravity = true
DepthBOR = 900                      # [m] depth below ground surface
InjectionRate = 2e-2                # [kg/sq/s]
InitialTemperature = 298.15         # [K]
InjectionPressure = 16e6            # [Pa]
InjectionTemperature = 305          # [K]

[Brine]
Salinity = 0

[SpatialParams]
BrooksCoreyPcEntry = 1e3
BrooksCoreyLambda = 2.0
Swr = 0.0
Snr = 0.0


[Newton]
MaxRelativeShift = 1e-6


[Component]
SolidDensity = 2700
SolidThermalConductivity = 2.8
SolidHeatCapacity = 790


