// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup CO2Tests
 * \brief Definition of a problem, where CO2 is injected in a reservoir.
 */

#ifndef DUMUX_HETEROGENEOUS_PROBLEM_HH
#define DUMUX_HETEROGENEOUS_PROBLEM_HH

#include <dune/grid/common/partitionset.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>

#include <dumux/parallel/vectorcommdatahandle.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/discretization/box/scvftoscvboundarytypes.hh>
#include <dumux/material/components/co2.hh>

#include "co2tables.hh"

namespace Dumux {

/*!
 * \ingroup CO2Tests
 * \brief Definition of a problem, where CO2 is injected in a reservoir.
 *
 * The domain is sized 200m times 100m and consists of four layers, a
 * permeable reservoir layer at the bottom, a barrier rock layer with reduced
 * permeability, another reservoir layer and at the top a barrier rock layer
 * with a very low permeablility.
 *
 * CO2 is injected at the permeable bottom layer from the left side.
 * The domain is initially filled with brine.
 *
 * The grid is unstructered and permeability and porosity for the elements are
 * read in from the grid file. The grid file also contains so-called boundary
 * IDs which can be used assigned during the grid creation in order to differentiate
 * between different parts of the boundary.
 * These boundary ids can be imported into the problem where the boundary
 * conditions can then be assigned accordingly.
 *
 * The model is able to use either mole or mass fractions. The property useMoles
 * can be set to either true or false in the problem file. Make sure that the
 * according units are used in the problem setup.
 * The default setting for useMoles is false.
 *
 * To run the simulation execute the following line in shell (works with the
 * box and cell centered spatial discretization method):
 * <tt>./test_ccco2 </tt> or <tt>./test_boxco2 </tt>
 */
template <class TypeTag >
class HeterogeneousProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;
    using VolumeVariables = typename GridVariables::GridVolumeVariables::VolumeVariables;

    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using Indices = typename ModelTraits::Indices;

    // copy some indices for convenience
    enum
    {
        // primary variable indices
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,

        // phase presence index
        firstPhaseOnly = Indices::firstPhaseOnly,

        // component indices
        BrineIdx = FluidSystem::BrineIdx,
        CO2Idx = FluidSystem::CO2Idx,

        // equation indices
        conti0EqIdx = Indices::conti0EqIdx,
        contiCO2EqIdx = conti0EqIdx + CO2Idx
    };

    #if !ISOTHERMAL
      enum
      {
        temperatureIdx = Indices::temperatureIdx,
        energyEqIdx = Indices::energyEqIdx,
      };
    #endif

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;

    using CO2 = Components::CO2<Scalar, HeterogeneousCO2Tables::CO2Tables>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;

    // Time Loop Pointer
    using TimeLoopPtr = std::shared_ptr<TimeLoop<Scalar>>;

    //! Property that defines whether mole or mass fractions are used
    static constexpr bool useMoles = ModelTraits::useMoles();

    // the discretization method we are using
    static constexpr auto discMethod = GetPropType<TypeTag, Properties::GridGeometry>::discMethod;
    static constexpr bool isBox = GridGeometry::discMethod == DiscretizationMethod::box;

    // world dimension to access gravity vector
    static constexpr int dimWorld = GridView::dimensionworld;

public:
    template<class SpatialParams>
    HeterogeneousProblem(std::shared_ptr<const GridGeometry> gridGeometry, std::shared_ptr<SpatialParams> spatialParams)
    : ParentType(gridGeometry, spatialParams)
    {
        nTemperature_       = getParam<int>("FluidSystem.NTemperature");
        nPressure_          = getParam<int>("FluidSystem.NPressure");
        pressureLow_        = getParam<Scalar>("FluidSystem.PressureLow");
        pressureHigh_       = getParam<Scalar>("FluidSystem.PressureHigh");
        temperatureLow_     = getParam<Scalar>("FluidSystem.TemperatureLow");
        temperatureHigh_    = getParam<Scalar>("FluidSystem.TemperatureHigh");
        depthBOR_           = getParam<Scalar>("Problem.DepthBOR");
        name_               = getParam<std::string>("Problem.Name");
        injectionRate_      = getParam<Scalar>("Problem.InjectionRate");
        initialTemperature_ = getParam<Scalar>("Problem.InitialTemperature");
        orderTimeScheme_    = getParam<int>("TimeScheme.Order", 1);

        #if !ISOTHERMAL
          injectionPressure_ = getParam<Scalar>("Problem.InjectionPressure");
          injectionTemperature_ = getParam<Scalar>("Problem.InjectionTemperature");
        #endif

        // initialize the tables of the fluid system
        FluidSystem::init(/*Tmin=*/temperatureLow_,
                          /*Tmax=*/temperatureHigh_,
                          /*nT=*/nTemperature_,
                          /*pmin=*/pressureLow_,
                          /*pmax=*/pressureHigh_,
                          /*np=*/nPressure_);

        // stating in the console whether mole or mass fractions are used
        if (gridGeometry->gridView().comm().rank() == 0)
        {
            if (useMoles)
                std::cout << "-- Problem uses mole fractions." << std::endl;
            else
                std::cout << "-- Problem uses mass fractions." << std::endl;
        }

        // precompute the boundary types for the box method from the cell-centered boundary types
        scvfToScvBoundaryTypes_.computeBoundaryTypes(*this);
    }

    /*!
     * \brief Appends all quantities of interest which can be derived
     *        from the solution of the current time step to the VTK
     *        writer.
     */
    template<class VTKWriter>
    void addFieldsToWriter(VTKWriter& vtk)
    {
        const auto numElements = this->gridGeometry().gridView().size(0);
        const auto numDofs = this->gridGeometry().numDofs();

        vtkKxx_.resize(numElements);
        vtkPorosity_.resize(numElements);
        vtkBoxVolume_.resize(numDofs, 0.0);

        vtk.addField(vtkKxx_, "Kxx");
        vtk.addField(vtkPorosity_, "cellwisePorosity");
        vtk.addField(vtkBoxVolume_, "boxVolume");

        #if !ISOTHERMAL
          vtk.addVolumeVariable([](const VolumeVariables& v){ return v.enthalpy(BrineIdx); }, "enthalpyW");
          vtk.addVolumeVariable([](const VolumeVariables& v){ return v.enthalpy(CO2Idx); }, "enthalpyN");
        #else
          vtkTemperature_.resize(numDofs, 0.0);
          vtk.addField(vtkTemperature_, "T");
        #endif

        const auto& gridView = this->gridGeometry().gridView();
        for (const auto& element : elements(gridView, Dune::Partitions::interior))
        {
            const auto eIdx = this->gridGeometry().elementMapper().index(element);
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            for (const auto& scv : scvs(fvGeometry))
            {
                const auto dofIdxGlobal = scv.dofIndex();
                vtkBoxVolume_[dofIdxGlobal] += scv.volume();
                #if ISOTHERMAL
                  vtkTemperature_[dofIdxGlobal] = initialTemperatureField_(scv.dofPosition());
                #endif
            }

            vtkKxx_[eIdx] = this->spatialParams().permeability(eIdx);
            vtkPorosity_[eIdx] = 1- this->spatialParams().inertVolumeFraction(eIdx);
        }

        // communicate box volume at process boundaries for vertex-centered scheme (box)
        if constexpr (isBox)
        {
            if (gridView.comm().size() > 1)
            {
                VectorCommDataHandleSum<typename GridGeometry::VertexMapper, std::vector<Scalar>, GridView::dimension>
                sumVolumeHandle(this->gridGeometry().vertexMapper(), vtkBoxVolume_);
                gridView.communicate(sumVolumeHandle,
                                     Dune::InteriorBorder_InteriorBorder_Interface,
                                     Dune::ForwardCommunication);
            }
        }
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }

    // Set the timeloop
    void setTimeLoop(TimeLoopPtr timeLoop)
    { timeLoop_ = timeLoop; }

    // Return time of simulation
    Scalar time() const
    { return timeLoop_->time(); }

    // Return order of Euler Time Scheme
    int getTimeSchemeOrder() const
    {
      return orderTimeScheme_;
    }

    /*!
     * \brief Returns the temperature within the domain.
     *
     * \param globalPos The global position
     *
     * This problem assumes a geothermal gradient with
     * a surface temperature of 10 degrees Celsius.
     */
    Scalar temperatureAtPos(const GlobalPosition &globalPos) const
    { return initialTemperatureField_(globalPos); }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    BoundaryTypes  boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes bcTypes;

        double eps_ = 1.0e-1;
        if( globalPos[0] > 600-eps_ )
        {
            bcTypes.setAllDirichlet();
        }
        else
            bcTypes.setAllNeumann();

        return bcTypes;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
     *
     * \return the Dirichlet values for the conservation equations in
     *               \f$ [ \textnormal{unit of primary variable} ] \f$
     * \param globalPos The global position
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    { return initial_(globalPos); }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann boundary segment.
     *
     * This is the method for the case where the Neumann condition is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param elemFluxVarsCache Flux variables caches for all faces in stencil
     * \param scvf The sub-control volume face
     *
     * For this method, the \a values parameter stores the flux
     * in normal direction of each phase. Negative values mean influx.
     * E.g. for the mass balance that would the mass flux in \f$ [ kg / (m^2 \cdot s)] \f$.
     */
    NumEqVector neumannAtPos(const GlobalPosition &globalPos) const
    {
       NumEqVector fluxes(0.0);
       double eps_ = 1.0e-1;
       Scalar M_CO2 = 4.4e-2; //mol.kg^{-1}
       if (globalPos[0] < eps_ && globalPos[2] < 12.5 && globalPos[2] > 0 )
       {
           fluxes[contiCO2EqIdx] = - injectionRate_ / M_CO2 / 1.25; //on injecte la meme quantité qu(avant sur une face plus petit
           //fluxes[contiCO2EqIdx] = -injectionRate_ / M_CO2;
           #if !ISOTHERMAL
             // energy fluxes are always mass specific
             fluxes[energyEqIdx] = - injectionRate_ /*kg/(m^2 s)*/*CO2::gasEnthalpy( injectionTemperature_, injectionPressure_)/1.25/*J/kg*/; // W/(m^2)
           #endif
       }

       return fluxes;
    }
    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluates the initial values at a position.
     *
     * \param globalPos The global position
     * \return The initial values for the conservation equations in
     *           \f$ [ \textnormal{unit of primary variables} ] \f$
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        return initial_(globalPos);
    }

    // \}

    // ------------ //
    // --  Files -- //
    // ------------ //

    // Initialize Solubility and Time Files
    void initFiles(int restart, int iteration)
    {
      if(restart)
      {
        delete_line("time.dat", iteration);
        timeFile = new std::ofstream("time.dat",std::ios::app);

        delete_line("solubility.dat", iteration);
        solubilityFile = new std::ofstream("solubility.dat",std::ios::app);
      }
      else
      {
        // Solubility
        solubilityFile = new std::ofstream("solubility.dat",std::ios::out);
        *solubilityFile << "Time" << "\t" << "Solubility_L" << "\t" << "Solubility_G" << "\t" << "Total_Qty_CO2" << "\t"<< "Dissolution_Rate"<< std::endl;

        // Time
        timeFile = new std::ofstream("time.dat",std::ios::out);
        *timeFile << "Time" << "\t" << "Timestep Size" << "\t" << "Number of Newton Iteration" << std::endl;
      }

      dataFile = new std::ofstream("data.dat", std::ios::out);
      *dataFile << "Rayleigh Solutal" << "\t" << "Rayleigh Thermal" << "\t" << "OnsetTime" << "\t" << "Wavelength" << std::endl;
    }

    // Delete n-th line from file
    void delete_line(const char *file_name, int iteration)
    {
      // open file in read mode or in mode
      std::ifstream ifs(file_name);

      // open file in write mode or out mode
      std::ofstream ofs;
      ofs.open("temp.dat", std::ios::out);

      // loop getting lines
      int countLines = 1;
      if(ifs.is_open())
      {
        std::string line;
        while (std::getline(ifs, line) && countLines <= iteration+1)
        {
          ofs << line << std::endl;
          countLines++;
        }
      }
      // closing output file
      ofs.close();

      // closing input file
      ifs.close();

      // remove the original file
      std::remove(file_name);

      // rename the file
      std::rename("temp.dat", file_name);
    }

    // Compute Solubility
    void computesolubility(const SolutionVector &curSol)
    {
      const auto& gridView = this->gridGeometry().gridView();

      double solubilityL = 0.;
      double solubilityG = 0.;
      double meanC = 0.;
      double dissolution_rate = 0.;
      double volume = 0.;

      for (const auto& element : elements(gridView, Dune::Partitions::interior))
      {
        auto elemSol = elementSolution(element, curSol, this->gridGeometry());

        auto fvGeometry = localView(this->gridGeometry());
        fvGeometry.bindElement(element);

        for (auto&& scv : scvs(fvGeometry))
        {
          VolumeVariables volVars;
          volVars.update(elemSol, *this, element, scv);

          // Calcul Solubility
          solubilityL += volVars.porosity()*volVars.molarDensity(0)*volVars.saturation(0)*volVars.moleFraction(0,1)*scv.volume();
          solubilityG += volVars.porosity()*volVars.molarDensity(1)*volVars.saturation(1)*volVars.moleFraction(1,1)*scv.volume();

          // Calcul Dissolution Rate
          const auto& globalPos = scv.dofPosition();
          // if(globalPos[1] <= heightLayer_)
          // {
            meanC += volVars.porosity() * volVars.molarDensity(0)*volVars.saturation(0)*volVars.moleFraction(0,1)*scv.volume();
            volume += scv.volume();
          // }
        }
      }

      if (gridView.comm().size() > 0)
      {
        solubilityL = gridView.comm().sum(solubilityL);
        solubilityG = gridView.comm().sum(solubilityG);

        meanC = gridView.comm().sum(meanC);
        volume = gridView.comm().sum(volume);
      }


      if (gridView.comm().rank() == 0)
      {
        //std::cout << "meanC =" << meanC_prev_ << " " << meanC << " " << timeLoop_->previousTimeStepSize() << '\n';

        dissolution_rate = 0.04401 /** heightLayer_*/ * (meanC - meanC_prev_)/(timeLoop_->previousTimeStepSize()*volume/(365*24*3600) );

        // Save previous mean Concentration
        meanC_prev_ = meanC;

        // Write on File
        *solubilityFile << time()/(365*24*3600) <<"\t"<< solubilityL <<"\t"<< solubilityG <<"\t"<< solubilityL + solubilityG <<"\t"<< dissolution_rate << std::endl;
      }
    }

    // Compute Data
    void computeData(const SolutionVector &curSol)
    {
      Scalar Rayleigh_Solutal = 0.;
      Scalar Rayleigh_Thermal = 0.;
      Scalar onsetTime = 0.;
      Scalar wavelength = 0.;

      const auto& gridView = this->gridGeometry().gridView();

      for (const auto& element : elements(gridView, Dune::Partitions::interior))
      {
        auto elemSol = elementSolution(element, curSol, this->gridGeometry());

        auto fvGeometry = localView(this->gridGeometry());
        fvGeometry.bindElement(element);

        for (auto&& scv : scvs(fvGeometry))
        {
          VolumeVariables volVars;
          volVars.update(elemSol, *this, element, scv);
          const auto& globalPos = scv.dofPosition();

          Scalar porosity = volVars.porosity();
          Scalar permeability = volVars.permeability();
          Scalar brine_viscosity = volVars.viscosity(0);
          Scalar brine_diffusivity = volVars.diffusionCoefficient(0,BrineIdx, CO2Idx);
          Scalar diff_density = volVars.molarDensity(0) - volVars.molarDensity(1);

          onsetTime  = 48.7 * pow((porosity * brine_viscosity * sqrt(brine_diffusivity)/(370 * this->spatialParams().gravity(globalPos)[dimWorld-1] * permeability )),2);

          wavelength = (2 * M_PI / 0.057) * (porosity * brine_viscosity) / (370 * this->spatialParams().gravity(globalPos)[dimWorld-1] * permeability);

        }
      }

      *dataFile << Rayleigh_Solutal << "\t" << Rayleigh_Thermal << "\t" << onsetTime << "\t" << wavelength << std::endl;

      // Rayleigh Thermal

      // Rayleigh Solutal


      // OnsetTime
      // Scalar porosity = getParam<Scalar>("SpatialParams.Porosity");
      // Scalar permability = getParam<Scalar>("SpatialParams.Permeability");
      // Scalar brine_viscosity =  FluidSystem::BrineCO2::viscosity(0, 1);
      //Scalar brine_diffusivity = FluidSystem::BrineCO2::binaryDiffusionCoefficient(0, 0, BrineIdx, CO2Idx);
      // Scalar density_diff =

      // std::cout << "brine_viscosity = " << brine_viscosity << '\n';
      // std::cout << "brine_diffusivity = " << brine_diffusivity << '\n';

      // Scalar tc = 48.7 * pow( (porosity * brine_viscosity * sqrt(brine_diffusivity)) /  ,2);

      // Wavelength
    }

    // Compute Time Data
    void computeTimeData(Scalar time, Scalar timeStepSize, int newtonIterations)
    {
      *timeFile << time << "\t" << timeStepSize << "\t" << newtonIterations << std::endl;
    }

    // Close Files
    void closeFiles()
    {
      solubilityFile->close();
      timeFile->close();
      dataFile->close();
    }

    // ---------------- //
    // --  Functions -- //
    // ---------------- //

    // Calculate Negative Distance to the interface layer of CO2
    double distanceToInterface(const GlobalPosition &globalPos) const
    {
       return globalPos[1]-heightLayer_;
    }

private:
    /*!
     * \brief Evaluates the initial values for a control volume.
     *
     * The internal method for the initial condition
     *
     * \param globalPos The global position
     */
    PrimaryVariables initial_(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);
        values.setState(firstPhaseOnly);
        Scalar temp = initialTemperatureField_(globalPos);
        Scalar densityW = 992;
        Scalar pl = 1e5 + ( depthBOR_ - globalPos[dimWorld-1] ) * densityW * 9.81;

        Scalar molardensity = 55333.33+1e-2+1e-3+0.1+1e-4+0.01;

        Scalar  moleFracLiquidco2 = 1e-2 / molardensity;

        //mole-fraction formulation
        values[Indices::switchIdx] = moleFracLiquidco2;
        values[Indices::pressureIdx] = pl;

        values[2] = std::log(1e-3/molardensity);  //h+
        values[3] = std::log(1e-1/molardensity);  //ca2+
        values[4] = std::log(1e-4/molardensity);  //Me+3
        values[5] = std::log(1e-2/molardensity);  //sio2
        values[6] = 0.1;  //calcite
        values[7] = 0.2;  //minA
        values[8] = 0.;   //minB;

        #if !ISOTHERMAL
          values[temperatureIdx] = temp;
        #endif

        return values;
    }

    Scalar initialTemperatureField_(const GlobalPosition globalPos) const
    {
        return initialTemperature_ + (depthBOR_ - globalPos[dimWorld-1]) *  0.03;
    }

    Scalar depthBOR_;
    Scalar injectionRate_;
    Scalar initialTemperature_;

    #if !ISOTHERMAL
      Scalar injectionPressure_, injectionTemperature_;
    #endif

    int nTemperature_;
    int nPressure_;

    std::string name_ ;

    Scalar pressureLow_, pressureHigh_;
    Scalar temperatureLow_, temperatureHigh_;

    int injectionTop_;
    int injectionBottom_;
    int dirichletBoundary_;
    int noFlowBoundary_;

    // Time Scheme
    int orderTimeScheme_;

    // Time
    TimeLoopPtr timeLoop_;

    // CO2 Layer
    Scalar heightLayer_;

    // Mean Concentrations
    Scalar meanC_prev_ = 0.;

    // vtk output
    std::vector<Scalar> vtkKxx_, vtkPorosity_, vtkBoxVolume_, vtkTemperature_;
    ScvfToScvBoundaryTypes<BoundaryTypes, discMethod> scvfToScvBoundaryTypes_;

    // File output
    std::ofstream *solubilityFile, *timeFile , *dataFile;
};

} // end namespace Dumux

#endif
