add_input_file_links()
dune_symlink_to_source_files(FILES grids)

# build target for the CO2 test problem
# Ignore the porosity for all box models since it is defined element-wise in these test
# but the default 2p2c implementation outputs porosity per vertex.
# Depending on the order of the elements, the porosity would differ in these cases.

dumux_add_test(NAME brunner-ni
               SOURCES main.cc
               LABELS porousmediumflow co2 co2ni
               COMPILE_DEFINITIONS TYPETAG=HeterogeneousNICCTpfa PUBLIC ISOTHERMAL=0
               CMAKE_GUARD "( dune-alugrid_FOUND )"
               CMAKE_GUARD HAVE_UMFPACK
               COMMAND ${CMAKE_SOURCE_DIR}/bin/testing/runtest.py
               CMD_ARGS --script fuzzy
                        --files ${CMAKE_SOURCE_DIR}/test/references/test_co2ni_tpfa-reference.vtu
                                ${CMAKE_CURRENT_BINARY_DIR}/test_co2ni_tpfa-00028.vtu
                        --command "${CMAKE_CURRENT_BINARY_DIR}/test_co2ni_tpfa paramsni.input -Problem.Name test_co2ni_tpfa")
                        
dumux_add_test(NAME brunner
               SOURCES main.cc
               LABELS porousmediumflow co2 co2ni
               COMPILE_DEFINITIONS TYPETAG=HeterogeneousCCTpfa PUBLIC ISOTHERMAL=1
               CMAKE_GUARD "( dune-alugrid_FOUND )"
               CMAKE_GUARD HAVE_UMFPACK
               COMMAND ${CMAKE_SOURCE_DIR}/bin/testing/runtest.py
               CMD_ARGS --script fuzzy
                        --files ${CMAKE_SOURCE_DIR}/test/references/test_co2ni_tpfa-reference.vtu
                                ${CMAKE_CURRENT_BINARY_DIR}/test_co2ni_tpfa-00028.vtu
                        --command "${CMAKE_CURRENT_BINARY_DIR}/test_co2ni_tpfa paramsni.input -Problem.Name test_co2ni_tpfa")
